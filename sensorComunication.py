import socket 
import time 
import pickle
from tinyec import registry
from Crypto.Cipher import AES
import hashlib, secrets, binascii

def encrypt_AES_GCM(msg, secretKey):
    aesCipher = AES.new(secretKey, AES.MODE_GCM)
    ciphertext, authTag = aesCipher.encrypt_and_digest(msg)
    return (ciphertext, aesCipher.nonce, authTag)

def decrypt_AES_GCM(ciphertext, nonce, authTag, secretKey):
    aesCipher = AES.new(secretKey, AES.MODE_GCM, nonce)
    plaintext = aesCipher.decrypt_and_verify(ciphertext, authTag)
    return plaintext

def ecc_point_to_256_bit_key(point):
    sha = hashlib.sha256(int.to_bytes(point.x, 32, 'big'))
    sha.update(int.to_bytes(point.y, 32, 'big'))
    return sha.digest()

curve = registry.get_curve('brainpoolP256r1')

def encrypt_ECC(msg, pubKey, bobPublicKey, privAlice):
    sharedECCKey = bobPublicKey * privAlice
    secretKey = ecc_point_to_256_bit_key(sharedECCKey)
    ciphertext, nonce, authTag = encrypt_AES_GCM(msg, secretKey)
    ciphertextPubKey = privKey * curve.g 
    return (ciphertext, nonce, authTag, ciphertextPubKey)

def decrypt_ECC(encryptedMsg, BobprivKey):
    (ciphertext, nonce, authTag, ciphertextPubKey) = encryptedMsg
    sharedECCKey = BobprivKey * ciphertextPubKey
    secretKey = ecc_point_to_256_bit_key(sharedECCKey)
    plaintext = decrypt_AES_GCM(ciphertext, nonce, authTag, secretKey)
    return plaintext

HEADERSIZE = 10

msglen = 0
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('192.168.1.107', 3044))

privKey = secrets.randbelow(curve.field.n) # alice
bobPrivKey = secrets.randbelow(curve.field.n) # bob

pubKey = privKey * curve.g # alice public Key

bobPubKey = bobPrivKey * curve.g # bobPubKey

sharedKey = bobPubKey * privKey

msg = b'La temperatura es : 275 K'
print("Texto a encriptar : ", msg)
encryptedMsg = encrypt_ECC(msg, pubKey, bobPubKey,privKey)
#print(encryptedMsg)
decrypt = decrypt_ECC(encryptedMsg, bobPrivKey)


encryptedMsgObj = {
    'ciphertext': encryptedMsg[0],
    'full' : decrypt,
    'sharedKey' : pubKey
}
#print("encrypted msg:", encryptedMsgObj)
msg = pickle.dumps(encryptedMsgObj)

msg = bytes(f"{len(msg):<{HEADERSIZE}}", 'utf-8') + msg

print("Texto encriptado : ", encryptedMsg[0])
s.send(msg)
